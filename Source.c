#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>

int fileDllOrExe(TCHAR *fileName);//verifica daca din linia de comanda este introdus un exe sau un dll
void processFile(TCHAR *fileName);
int _tmain(int argc, TCHAR *argv[]) {
	if (argc != 2) {
		printf("too many command arguments\n");
		return -1;
	}
	if (!fileDllOrExe(argv[1])) {
		printf("invalid command argument\n");
		return -1;
	}
	processFile(argv[1]);
	return 0;
}

int fileDllOrExe(TCHAR *fileName) {
	int lengthOfFileName = _tcslen(fileName);
	if (fileName[lengthOfFileName - 1 - 3] == '.') {
		if (!_tcsncmp(fileName + lengthOfFileName - 3, TEXT("dll"), 3) || !_tcsncmp(fileName + lengthOfFileName - 3, TEXT("exe"), 3)) {
			return 1;
		}
		return 0;
	}
	return 0;
}
//functie ce obtine FileAddress'ul pentru un RVA
int rvaToFa(PIMAGE_SECTION_HEADER sectionHeader, DWORD rva) {
	WORD i = 0;
	while (!((sectionHeader + i)->VirtualAddress <= rva && 
		(sectionHeader + i)->VirtualAddress + (sectionHeader + i)->Misc.VirtualSize > rva)) {
		i++;
	}
	DWORD offset = rva - (sectionHeader + i)->VirtualAddress;
	if (offset > (sectionHeader + i)->SizeOfRawData) {
		return -1;
	}
	return (sectionHeader + i)->PointerToRawData + offset;
}
void processFile(TCHAR *fileName) {
	HANDLE hFile;
	HANDLE hFileMapping;
	LPVOID lpFileBase;
	PIMAGE_DOS_HEADER dosHeader;

	hFile = CreateFile(fileName, GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Couldn't open file with CreateFile(),GLE = %d\n",GetLastError());
		return;
	}

	hFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hFileMapping == NULL)
	{
		CloseHandle(hFile);
		printf("Couldn't open file mapping with CreateFileMapping()\n");
		return;
	}

	lpFileBase = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
	if (lpFileBase == NULL)
	{
		CloseHandle(hFileMapping);
		CloseHandle(hFile);
		printf("Couldn't map view of file with MapViewOfFile()\n");
		return;
	}
	dosHeader = (PIMAGE_DOS_HEADER)lpFileBase;
	//folosesc (char*) pentru ca are incrementul 1
	PIMAGE_NT_HEADERS ntHeaders = (char*)lpFileBase + dosHeader->e_lfanew;
	PIMAGE_SECTION_HEADER sectionHeader = (char*)lpFileBase + dosHeader->e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER) + ntHeaders->FileHeader.SizeOfOptionalHeader;
	printf("File Header:\n");
	printf("-Machine:%x\n", ntHeaders->FileHeader.Machine);
	printf("-NumberOfSections:%x\n", ntHeaders->FileHeader.NumberOfSections);
	printf("-Characteristics:%x\n", ntHeaders->FileHeader.Characteristics);
	printf("Optional Header:\n");
	int fileAddress = rvaToFa(sectionHeader, ntHeaders->OptionalHeader.AddressOfEntryPoint);
	if (fileAddress == -1) {
		printf("-AddressOfEntryPoint:undef\n");
	}
	else {
		printf("-AddressOfEntryPoint:%x\n", fileAddress);
	}
	printf("-ImageBase:%x\n", ntHeaders->OptionalHeader.ImageBase);
	printf("-SectionAlignment:%x\n", ntHeaders->OptionalHeader.SectionAlignment);
	printf("-FileAlignment:%x\n", ntHeaders->OptionalHeader.FileAlignment);
	printf("-Subsystem:%x\n", ntHeaders->OptionalHeader.Subsystem);
	printf("-NumberOfRvaAndSizes:%x\n", ntHeaders->OptionalHeader.NumberOfRvaAndSizes);
	printf("Sections:\n");
	if (ntHeaders->FileHeader.NumberOfSections > 0) {
		for (int i = 0;i < ntHeaders->FileHeader.NumberOfSections;i++) {
			printf("%.8s,%x,%x\n", (sectionHeader + i)->Name, (sectionHeader + i)->PointerToRawData, (sectionHeader + i)->SizeOfRawData);
		}
	}
	printf("Exports:\n");
	if (ntHeaders->OptionalHeader.DataDirectory[0].VirtualAddress > 0) {
		int exportOffset = rvaToFa(sectionHeader, ntHeaders->OptionalHeader.DataDirectory[0].VirtualAddress);
		if (exportOffset != -1) {
			PIMAGE_EXPORT_DIRECTORY exportDirectory = (char*)lpFileBase + exportOffset;
			
			int offset1 = rvaToFa(sectionHeader,  exportDirectory->AddressOfNameOrdinals);
			if (offset1 == -1) {
				goto imp;
			}
			int offset2 = rvaToFa(sectionHeader, exportDirectory->AddressOfNames);
			if (offset2 == -1) {
				goto imp;
			}
			int offset3 = rvaToFa(sectionHeader, exportDirectory->AddressOfFunctions);
			if (offset3 == -1) {
				goto imp;
			}
			
			PWORD nameOrdinals = (char*)lpFileBase + offset1;
			PDWORD names = (char*)lpFileBase + offset2;
			PDWORD functions = (char*)lpFileBase + offset3;
			
			for (WORD i = 0;i < exportDirectory->NumberOfFunctions;i++) {
				BOOL found = FALSE;//folosita pentru a vedea daca exportul e cu nume sau nu
				for (WORD j = 0;j < exportDirectory->NumberOfNames;j++) {
					if (i == nameOrdinals[j]) {
						found = TRUE;
						int offset4 = rvaToFa(sectionHeader, names[j]);
						if (offset4 == -1) {
							goto imp;
						}
						char* name = (char*)lpFileBase + offset4;
						fileAddress = rvaToFa(sectionHeader, functions[i]);
						if (fileAddress == -1) {
							printf("%s,%x,undef\n", name, j);
						}
						else {
							printf("%s,%x,%x\n", name, j, fileAddress);
						}
					}
				}
				//export fara nume
				if (found == FALSE) {
					fileAddress = rvaToFa(sectionHeader, functions[i]);
					if (fileAddress == -1) {
						printf(",,undef\n");
					}
					else {
						printf(",,%x\n", fileAddress);
					}
				}
			}
		}
	}
imp:

	printf("Imports:\n");
	if (ntHeaders->OptionalHeader.DataDirectory[1].VirtualAddress > 0) {
		int importOffset = rvaToFa(sectionHeader, ntHeaders->OptionalHeader.DataDirectory[1].VirtualAddress);
		if (importOffset != -1) {
			PIMAGE_IMPORT_DESCRIPTOR importDescrpitor = (char*)lpFileBase + importOffset;
			int cnt = ntHeaders->OptionalHeader.DataDirectory[1].Size / sizeof(IMAGE_IMPORT_DESCRIPTOR);
			//cnt imi da cu 1 mai mare decat numarul actual al importurilor
			int i = 0;
			while (i < cnt - 1) {
				int nameOffset = rvaToFa(sectionHeader, (importDescrpitor + i)->Name);
				if (nameOffset == -1) {
					goto fin;
				}
				char* name = (char*)lpFileBase + nameOffset;
				//pentru verificarea daca importul se face in functie de nume sau de numarul de ordine folosesc direct
				//RVA, deoarece ma gandesc ca daca pe disk e de un anumit tip, acesta nu se va schimba cand e mapat in mem
				DWORD helper = 0x80000000;
				DWORD test = (importDescrpitor + i)->OriginalFirstThunk & helper;
				if (test == 0) {
					//import dupa nume
					int oftOffset = rvaToFa(sectionHeader, (importDescrpitor + i)->OriginalFirstThunk);
					if (oftOffset == -1) {
						goto fin;
					}
					PIMAGE_THUNK_DATA thunkData = (char*)lpFileBase + oftOffset;
					int j = 0;
					while ((thunkData + j)->u1.AddressOfData != NULL) {
						int impOffset = rvaToFa(sectionHeader, (thunkData + j)->u1.AddressOfData);
						if (impOffset == -1) {
							goto fin;
						}
						PIMAGE_IMPORT_BY_NAME importByName = (char*)lpFileBase + impOffset;
						printf("%s,%s\n", name, importByName->Name);
						j++;
					}
				}
				else {
					//import dupa ordinal
					int oftOffset = rvaToFa(sectionHeader, (importDescrpitor + i)->OriginalFirstThunk);
					if (oftOffset == -1) {
						goto fin;
					}
					PIMAGE_THUNK_DATA thunkData = (char*)lpFileBase + oftOffset;
					int j = 0;
					while ((thunkData + j)->u1.AddressOfData != NULL) {
						int impOffset = rvaToFa(sectionHeader, (thunkData + j)->u1.AddressOfData);
						if (impOffset == -1) {
							goto fin;
						}
						PIMAGE_IMPORT_BY_NAME importByName = (char*)lpFileBase + impOffset;
						printf("%s,%x\n", name, importByName->Hint);
						j++;
					}
				}

				i++;
			}
		}
		
	}
fin:
	UnmapViewOfFile(lpFileBase);
	CloseHandle(hFileMapping);
	CloseHandle(hFile);
}
